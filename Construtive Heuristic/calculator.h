
#ifndef POSTFIXCALC
#define POSTFIXCALC
#include <stdlib.h>

/* PostfixCalculator class:
*  Implements a post fix calculator as requested in the assignment
*/

class PostfixCalculator {
private:
      double* x;
      int n;
public:
      PostfixCalculator();
      ~PostfixCalculator();

      void push(double v);
      void pop();
      double top();
      bool empty();

      void add();
      void sub();
      void mul();
      void div();
      void neg();
};

#endif