#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>
#include <iomanip>
#include <vector>
#include "job.h"
#include "schedule.h"
using namespace std;

bool FILE_OPENED = false;
string filename;

Job* read_from_file(int* n) {
	static ifstream file;
	Job* MyJobs;
	int aux, j=0;

	if (!FILE_OPENED) {
		file.open(filename);

		if (!file.is_open()) {
			cout << "File not found." << endl;
			exit(1);
		} else {
			FILE_OPENED = true;
		}

		file >> *n;
	}

	file >> *n;

	MyJobs = new Job[*n];

	while (j<*n) {
		MyJobs[j].set_name(j+1);
		file >> aux;
		MyJobs[j].set_processing_time(aux);
		file >> aux;
		MyJobs[j].set_earliness_penalty(aux);
		file >> aux;
		MyJobs[j].set_tardiness_penalty(aux);
		j++;
	}

	return MyJobs;
}

void print_Jobs(Job MyJobs[], int n) {
	for (int i=0; i<n; i++) {
		cout << MyJobs[i].get_name() << " ";
		cout << MyJobs[i].get_processing_time() << " ";
		cout << MyJobs[i].get_earliness_penalty() << " ";
		cout << MyJobs[i].get_tardiness_penalty() << endl;
	}
}

Job* copy_jobs(Job MyJobs[], int n) {
	Job* MyJobs2 = new Job[n];
	for (int i=0; i<n; i++)
		MyJobs2[i] = MyJobs[i];
	return MyJobs2;
}

/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */
//																	//
//	compile with: g++ *.cpp -o main -Wall							//
//																	//
//	where cpp files are: main.cpp, job.cpp, schedule.cpp 			//
//	and calculator.cpp 												//
//																	//
/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */
//																	//
//	to run:	./executable schXYZ.txt 								//
//																	//
//	where schXYZ is one of sch10.txt, sch20.txt, ..., sch1000.txt	//
//																	//
/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */

int main(int argc, char** argv) {
	int n, due_date, test_before, test_after;
	int pointer_before=0, pointer_after=0;
	bool ALLOWED_TO_PROCEED;
	ofstream file_out, file2_out;

	string filename_z = argv[1];
	filename = "input_files/"+filename_z;

	Job* MyJobs = read_from_file(&n);
	Job* MyJobs2;

	string filename_out = "output_files/saida_C_"+filename_z;

	file_out.open(filename_out);

	file_out << "\th=0.2\th=0.4\th=0.6\th=0.8" << endl;
	file2_out << "\th=0.2\th=0.4\th=0.6\th=0.8" << endl;

	for (int k=0; k<10; k++) {
		file_out << "k = " << k+1 << "\t";
		file2_out << "k = " << k+1 << "\t";
		for (float h=0.2; h < 1.0; h+=0.2) {
			cout << ">>>>> >>>> >>> >> (k = " << k+1 << " | h = " << h << ") << <<< <<<< <<<<<" << endl;

			due_date = calc_due_date(MyJobs, h, n);		// calculate due date based on all available jobs and h
			set_due_date(MyJobs,due_date,n);			// update due date for every job 
			MyJobs2 = copy_jobs(MyJobs,n);

			order_by(MyJobs,"p A /", "decreasing", n);
			order_by(MyJobs2,"p B /", "decreasing", n);
			
			Schedule MySchedule(n,due_date);
			MySchedule.add_first_job(MyJobs2[0],due_date,'S');
			pointer_after++;
			
			while (MySchedule.get_number_of_scheduled_jobs() < n) {

				ALLOWED_TO_PROCEED = true;
				test_before = MySchedule.test_add_job(MyJobs[pointer_before],'X');
				test_after = MySchedule.test_add_job(MyJobs2[pointer_after],'Y');

				if (test_before == NOT_ABLE_TO_ALLOCATE && pointer_before < n) {
					pointer_before++;
					ALLOWED_TO_PROCEED = false;
				} else if (test_before == JOB_ALREADY_IN_SCHEDULE && pointer_before < n) {
					pointer_before++;
					ALLOWED_TO_PROCEED = false;
				}

				if (test_after == JOB_ALREADY_IN_SCHEDULE && pointer_after < n) {
					pointer_after++;
					ALLOWED_TO_PROCEED = false;
				}


				if (ALLOWED_TO_PROCEED) {
					if (pointer_before >= n) {
						MySchedule.add_job(MyJobs2[pointer_after],'Y');
						pointer_after++;
					} else {
						if (test_before <= test_after) {
							MySchedule.add_job(MyJobs[pointer_before],'X');
							pointer_before++;
						} else {
							MySchedule.add_job(MyJobs2[pointer_after],'Y');
							pointer_after++;
						}
					}
				}
			}
			pointer_before = 0;
			pointer_after = 0;

			cout << "Schedule:\t" << MySchedule.get_objective_function() << ": " << MySchedule << endl << endl;
			file_out << MySchedule.get_objective_function() << "\t";
		}
		file_out << endl;
		MyJobs = read_from_file(&n);
	}

	file_out.close();
		

	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */

	
	string files_out[7] = {"saida_C_sch10.txt", "saida_C_sch20.txt", "saida_C_sch50.txt", "saida_C_sch100.txt", "saida_C_sch200.txt", "saida_C_sch500.txt", "saida_C_sch1000.txt"};
	string files_comp[7] = {"biskup_10.txt", "biskup_20.txt", "biskup_50.txt", "biskup_100.txt", "biskup_200.txt", "biskup_500.txt", "biskup_1000.txt"};
	string my_f_out[7] = {"tabela_linda_C_10b.txt", "tabela_linda_C_20b.txt", "tabela_linda_C_50b.txt", "tabela_linda_C_100b.txt", "tabela_linda_C_200b.txt", "tabela_linda_C_500b.txt", "tabela_linda_C_1000b.txt"};

	string discard;
	ifstream f1, f2;
	ofstream f3;

	for (int i=0; i<7; i++) {
		files_out[i] = "output_files/" + files_out[i];
		my_f_out[i] = "output_files/" + my_f_out[i];
		files_comp[i] = "input_files/" + files_comp[i];
	}

	double f1_metrics[40], f2_metrics[40], comparison[40];
	double mean=0, sigma=0;
	
	int n_files;

	if (filename == "input_files/sch10.txt")
		n_files = 1;
	else if (filename == "input_files/sch20.txt")
		n_files = 2;
	else if (filename == "input_files/sch50.txt")
		n_files = 3;
	else if (filename == "input_files/sch100.txt")
		n_files = 4;
	else if (filename == "input_files/sch200.txt")
		n_files = 5;
	else if (filename == "input_files/sch500.txt")
		n_files = 6;
	else if (filename == "input_files/sch1000.txt")
		n_files = 7;
	else n_files = 0;

	for (int i=0; i<n_files; i++) {
		f1.open(files_out[i]);
		f2.open(files_comp[i]);
	
		for (int j=0; j<4; j++)
			f1 >> discard;

		for (int j=0; j<13; j++)
			f2 >> discard;

		for (int j=0; j<10; j++) {
			f1 >> discard >> discard >> discard;
			f2 >> discard >> discard >> discard;

			for (int k=0; k<4; k++) {
				f1 >> discard;
				f1_metrics[4*j+k] = stoi(discard);

				f2 >> discard;
				discard.erase(std::remove(discard.begin(), discard.end(), ','), discard.end());
				f2_metrics[4*j+k] = stoi(discard);
			}
		}

		f1.close();
		f2.close();
		
	}
	
	for (int j=0; j<10; j++) {
		for (int k=0; k<4; k++) {
			comparison[4*j+k] = (f1_metrics[4*j+k] - f2_metrics[4*j+k])/f2_metrics[4*j+k] * 100;
			mean += comparison[4*j+k];
		}
	}

	mean /= 40;

	for (int i=0; i<40; i++) {
		sigma += pow(mean-comparison[i],2);
	}

	sigma = sqrt(sigma/(40-1));

	cout << endl;
	cout << "N~(" << mean << "," << sigma << ")" << endl;

	std::cout.precision(2);
		
	cout << endl;
	f3.open(my_f_out[n_files-1]);

	for (int j=0; j<10; j++) {
		for (int k=0; k<4; k++) {
			f3 << std::setprecision(0) << std::fixed << "(" << f1_metrics[4*j+k] << "/" << f2_metrics[4*j+k] << ") " << std::setprecision(1) << comparison[4*j+k] << "%\t";
		}
		f3 << endl;
	}

	f3.close();
	
	delete[] MyJobs;
	delete[] MyJobs2;

	return 0;
	
}