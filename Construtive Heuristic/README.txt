Please keep the original file structure:

zip_folder
  /input_files 		(where sch*.txt and biskup*.txt files are)
  /output_files 	(where output files will be written)
  /cpp files		(project files)
  /header files		(project files)
  /README.txt 		(current file: informational)


*	compile with: g++ *.cpp -o main -Wall
	where cpp files are: main.cpp, job.cpp, schedule.cpp and calculator.cpp 	


*	to run:	./executable schXYZ.txt
	where schXYZ is one of sch10.txt, sch20.txt, ..., sch1000.txt