#include <iostream>
#include "calculator.h"

// PostfixCalculator constructor
PostfixCalculator::PostfixCalculator() {
      x = nullptr;
      n = 0;
}

// PostfixCalculator destructor
PostfixCalculator::~PostfixCalculator() {
      delete [] x;
}

// PostfixCalculator push: add element v to stack
void PostfixCalculator::push(double v) {
      double* temp = new double[n+1];
      for (int i = 0; i < n; i++)
            temp[i] = x[i];
      temp[n] = v;
      n++;
      delete [] x;
      x = temp;
}

// PostfixCalculator pop: pop stack element
void PostfixCalculator::pop() {
      if (!empty())
            n--;
      else {
            std::cout << "stack ended!" << std::endl;
            exit(-1);
      }
}

// PostfixCalculator top: return stack element
double PostfixCalculator::top() {
      if (!empty())
            return x[n-1];
      else {
            std::cout << "stack ended!" << std::endl;
            exit(-1);
      }
}

// PostfixCalculator empty: return true if stack is empty
bool PostfixCalculator::empty() {
      if (n>0)
            return false;
      else
            return true;
}

// PostfixCalculator add: add two elements
void PostfixCalculator::add() {
      double w = top();
      pop();
      double y = top();
      pop();

      push(y + w);
}

// PostfixCalculator sub: subtract two elements
void PostfixCalculator::sub() {
      double w = top();
      pop();
      double y = top();
      pop();

      push(y - w);
}

// PostfixCalculator mul: multiply two elements
void PostfixCalculator::mul() {
      double w = top();
      pop();
      double y = top();
      pop();

      push(y * w);
}

// PostfixCalculator div: divide two elements
void PostfixCalculator::div() {
      double w = top();
      pop();
      double y = top();
      pop();

      push(y/w);
}

// PostfixCalculator neg: negate stack element
void PostfixCalculator::neg() {
      double w = top();
      pop();
      
      push(-w);
}