#include "schedule.h"

Schedule::Schedule(int n_jobs, int d) {
	n = n_jobs;
	due_date = d;
	number_of_scheduled_jobs = 0;
	number_of_anticipated_jobs = 0;
	//head = nullptr;
	//tail = nullptr;
}

Schedule::Schedule() {};
Schedule::~Schedule() {};

void Schedule::set_due_date(int d) {
	for (std::list<Job>::iterator it=head; it != tail; ++it)
		it->set_due_date(d);
}
void Schedule::add_first_job(Job first_job, int time, char mode) {
	if (mode == 'C') {
		first_job.set_completion_time(time);
		
		if (first_job.get_starting_time() < 0)
			first_job.set_starting_time(0);

		MySchedule.push_back(first_job);
		head = MySchedule.begin();
		tail = MySchedule.end();
		number_of_scheduled_jobs++;
		number_of_anticipated_jobs++;

	} else if (mode == 'S') {
		if (time < 0)
			time = 0;
		
		first_job.set_starting_time(time);

		MySchedule.push_back(first_job);
		head = MySchedule.begin();
		tail = MySchedule.end();
		number_of_scheduled_jobs++;

	} else {
		std::cout << "Select mode 'C' for >> completion time << or 'S' for >> starting time <<" << std::endl;
	}
}

int Schedule::get_number_of_scheduled_jobs() {
	return number_of_scheduled_jobs;
}




void Schedule::add_job(Job job, char mode) {
	int st,ct;
	if (mode == 'B') {
		st = MySchedule.front().get_starting_time();
		job.set_completion_time(st);
		MySchedule.push_front(job);
		head = MySchedule.begin();
		number_of_scheduled_jobs++;
		number_of_anticipated_jobs++;

	} else if (mode == 'A') {
		ct = MySchedule.back().get_completion_time();
		job.set_starting_time(ct);
		MySchedule.push_back(job);
		tail = MySchedule.end();
		number_of_scheduled_jobs++;

	} else if (mode == 'X') {
		for (std::list<Job>::iterator it=head; it != tail; ++it) {
			if (it->get_starting_time() == due_date) {
				MySchedule.insert(it,job);
				head = MySchedule.begin();

				it--;

				it->set_completion_time(due_date);
				st = it->get_starting_time();

				for (std::list<Job>::iterator it2=it; it2 != head; --it2) {
					if (it2 == it)
						continue;
					else {
						it2->set_completion_time(st);
						st = it2->get_starting_time();
					}
				}
				if (number_of_scheduled_jobs > 1)
					head->set_completion_time(st);
				number_of_scheduled_jobs++;
				break;
			}
		}
		number_of_anticipated_jobs++;
	} else if (mode == 'Y') {
		for (std::list<Job>::iterator it=head; it != tail; ++it) {
			if (it->get_starting_time() == due_date) {
				MySchedule.insert(it,job);
				//head = MySchedule.begin();
				tail = MySchedule.end();
				it--;

				it->set_starting_time(due_date);
				st = it->get_completion_time();

				for (std::list<Job>::iterator it2=it; it2 != tail; ++it2) {
					if (it2 == it)
						continue;
					else {
						it2->set_starting_time(st);
						st = it2->get_completion_time();
					}
				}
				//if (number_of_scheduled_jobs > 1)
				//	tail->set_starting_time(st);
				number_of_scheduled_jobs++;
				break;
			}
		}
	}

	else {
		std::cout << "Select mode 'A' for >> after last job << or 'B' for >> before first job <<" << std::endl;
	}
}

bool Schedule::job_in_schedule(Job job) {
	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		//std::cout << "iterator job name: " << it->get_name();
		//std::cout << "parameter job name: " << job.get_name() << std::endl;
		if (it->get_name() == job.get_name())
			return true;
	}
	return false;
}
int Schedule::test_add_job(Job job, char mode){
	int increase = 0;

	if (job_in_schedule(job))
		return JOB_ALREADY_IN_SCHEDULE;

	if (mode == 'B') {
		if (MySchedule.front().get_starting_time() - job.get_processing_time() >= 0)
			increase = (due_date - MySchedule.front().get_starting_time())*job.get_earliness_penalty();
		else
			return NOT_ABLE_TO_ALLOCATE;
	} else if (mode == 'X') {
		if (MySchedule.front().get_starting_time() - job.get_processing_time() >= 0) {
			for (std::list<Job>::iterator it=head; it != tail; ++it) {
				if (it->get_completion_time() <= due_date)
					increase += job.get_processing_time()*it->get_earliness_penalty();
				else
					break;
			}
		} else
			return NOT_ABLE_TO_ALLOCATE;
	} else if (mode == 'Y') {
		increase = job.get_processing_time()*job.get_tardiness_penalty();
		for (std::list<Job>::iterator it=head; it != tail; ++it) {
			if (it->get_completion_time() > due_date)
				increase += job.get_processing_time()*it->get_tardiness_penalty();
			else
				continue;
		}	
	}
	else {
		increase = (MySchedule.back().get_completion_time()+job.get_processing_time()-due_date)*job.get_tardiness_penalty();
	}

	return increase;
}



long int Schedule::get_objective_function() {
	long int sum = 0;
	
	head = MySchedule.begin();
	tail = MySchedule.end();

	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		if (it->get_completion_time() < due_date) {
			sum += (due_date - it->get_completion_time())*it->get_earliness_penalty();
		} else {
			sum += (it->get_completion_time() - due_date)*it->get_tardiness_penalty();
		}
	}

    return sum;
}

void Schedule::fix_pointers() {
	head = MySchedule.begin();
	tail = MySchedule.end();
}




void Schedule::swap(int id_a, int id_b) {
	Job Ja,Jb;

	head = MySchedule.begin();
	tail = MySchedule.end();

	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		if (it->get_name() == id_a)
			Ja = *it;
		if (it->get_name() == id_b)
			Jb = *it;
	}

	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		if (it->get_name() == id_a) {
			it->set_name(Jb.get_name());
			it->set_processing_time(Jb.get_processing_time());
			it->set_earliness_penalty(Jb.get_earliness_penalty());
			it->set_tardiness_penalty(Jb.get_tardiness_penalty());
		}
		else if (it->get_name() == id_b) {
			it->set_name(Ja.get_name());
			it->set_processing_time(Ja.get_processing_time());
			it->set_earliness_penalty(Ja.get_earliness_penalty());
			it->set_tardiness_penalty(Ja.get_tardiness_penalty());
		}
	}

	// SWITCHED JOBS:
	
	//std::cout << Ja << std::endl;
	//std::cout << Jb << std::endl;
	
	starting_time = fmax(head->get_starting_time() - (Jb.get_processing_time() - Ja.get_processing_time()),0);
	//std::cout << "Starting time now is: " << starting_time << std::endl;
	
	update_times();
}

void Schedule::update_times() {
	head = MySchedule.begin();
	tail = MySchedule.end();
	int mobile_st = starting_time;

	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		it->set_starting_time(mobile_st);
		it->set_completion_time(mobile_st + it->get_processing_time());
		mobile_st += it->get_processing_time();
	}
}

int Schedule::get_starting_time() {
	return MySchedule.begin()->get_starting_time();
}

void Schedule::apply_Vshape() {
	std::list<Job>::iterator aux1;
	std::list<Job>::iterator aux2;
	aux1 = tail;
	Job swap;

	for (std::list<Job>::iterator it=head; it != tail; ++it) {
		aux2 = it;
		for (std::list<Job>::iterator it2=++aux2; it2 != aux1; ++it2) {
			//std::cout << "Comparando Job " << it->get_name() << " and " << it2->get_name() << std::endl;
			if (it->get_completion_time() < due_date && it2->get_completion_time() < due_date) {
				if (it->get_p_over_alpha() < it2->get_p_over_alpha()) {
					swap.set_name(it->get_name());
					swap.set_processing_time(it->get_processing_time());
					swap.set_earliness_penalty(it->get_earliness_penalty());
					swap.set_tardiness_penalty(it->get_tardiness_penalty());

					it->set_name(it2->get_name());
					it->set_processing_time(it2->get_processing_time());
					it->set_earliness_penalty(it2->get_earliness_penalty());
					it->set_tardiness_penalty(it2->get_tardiness_penalty());

					it2->set_name(swap.get_name());
					it2->set_processing_time(swap.get_processing_time());
					it2->set_earliness_penalty(swap.get_earliness_penalty());
					it2->set_tardiness_penalty(swap.get_tardiness_penalty());
				}
			} else if (it->get_completion_time() > due_date && it2->get_completion_time() > due_date) {
				if (it->get_p_over_beta() > it2->get_p_over_beta()) {
					swap.set_name(it->get_name());
					swap.set_processing_time(it->get_processing_time());
					swap.set_earliness_penalty(it->get_earliness_penalty());
					swap.set_tardiness_penalty(it->get_tardiness_penalty());

					it->set_name(it2->get_name());
					it->set_processing_time(it2->get_processing_time());
					it->set_earliness_penalty(it2->get_earliness_penalty());
					it->set_tardiness_penalty(it2->get_tardiness_penalty());

					it2->set_name(swap.get_name());
					it2->set_processing_time(swap.get_processing_time());
					it2->set_earliness_penalty(swap.get_earliness_penalty());
					it2->set_tardiness_penalty(swap.get_tardiness_penalty());
				}
			}
		}
	}
	update_times();
}


std::vector<int> Schedule::get_jobs(std::string mode) {
	std::vector<int> id_jobs;
	if (mode == "early") {
		for (std::list<Job>::iterator it=head; it != tail; ++it)
			if (it->get_completion_time() < due_date)
				id_jobs.push_back(it->get_name());

		std::reverse(id_jobs.begin(), id_jobs.end());
	} else if (mode == "late") {
		for (std::list<Job>::iterator it=head; it != tail; ++it)
			if (it->get_completion_time() > due_date)
				id_jobs.push_back(it->get_name());
	}


	return id_jobs;
}

std::list<Job> Schedule::get_list() {
	return MySchedule;
}

std::vector<int> Schedule::get_job_names() {
	std::vector<int> id_jobs;
	
	for (std::list<Job>::iterator it=head; it != tail; ++it)
		id_jobs.push_back(it->get_name());

	return id_jobs;
}

std::vector<Job> Schedule::get_list_of_jobs() {
	std::vector<Job> jobs;
	
	for (std::list<Job>::iterator it=head; it != tail; ++it)
		jobs.push_back(*it);

	return jobs;
}

/*
std::list<Job>::iterator Schedule::get_schedule_initial_position() const {
	return MySchedule.begin(); 
}

std::list<Job>::iterator Schedule::get_schedule_last_position() const {
	return MySchedule.end(); 
}
*/
std::ostream& operator<< (std::ostream& stream, const Schedule& schedule) {
	
	//stream << "{" << job.name << ": " << job.starting_time << ", " << job.processing_time << ", " << job.completion_time << "}";

	//stream << "Schedule in the following format:" << std::endl << "{job id: starting time, processing time, completion time}:" << std::endl << std::endl;
	for (std::list<Job>::iterator it=schedule.head; it != schedule.tail; ++it)
    	stream << (*it).get_name() << ' ';

	return stream;
}

Schedule& Schedule::operator=(const Schedule& operand) {
	
	if (&operand == this)
		return *this;

	MySchedule = operand.MySchedule;
	head = MySchedule.begin();
	tail = MySchedule.end();
	n = operand.n;
	due_date = operand.due_date;
	starting_time = operand.starting_time;
	number_of_scheduled_jobs = operand.number_of_scheduled_jobs;
	number_of_anticipated_jobs = operand.number_of_anticipated_jobs;

	return *this;
}