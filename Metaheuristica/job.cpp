#include "job.h"
#include "calculator.h"

Job::Job(){
	name = NOT_INITIALIZED;
	due_date = NOT_INITIALIZED;
	processing_time = NOT_INITIALIZED;
	earliness_penalty = NOT_INITIALIZED;
	tardiness_penalty = NOT_INITIALIZED;
	starting_time = NOT_INITIALIZED;
	completion_time = NOT_INITIALIZED;
};

Job::Job(int n, int d, int pi, int Ai, int Bi) {
	name = n;
	due_date = d;
	processing_time = pi;
	earliness_penalty = Ai;
	tardiness_penalty = Bi;
}

Job::~Job(){};

void Job::set_name(int n) {
	name = n;
}

void Job::set_due_date(int d) {
	due_date = d;
}

void Job::set_processing_time(int pi) {
	processing_time = pi;
}

void Job::set_earliness_penalty(int Ai) {
	earliness_penalty = Ai;
}

void Job::set_tardiness_penalty(int Bi) {
	tardiness_penalty = Bi;
}

void Job::set_starting_time(int s) {
	starting_time = s;
	completion_time = starting_time + processing_time;
}

void Job::set_completion_time(int C) {
	completion_time = C;
	starting_time = completion_time - processing_time;
}

int Job::get_name() {
	return name;
}

int Job::get_due_date() {
	return due_date;
}

int Job::get_processing_time() {
	return processing_time;
}

int Job::get_earliness_penalty() {
	return earliness_penalty;
}

int Job::get_tardiness_penalty() {
	return tardiness_penalty;
}

int Job::get_starting_time() {
	return starting_time;
}

int Job::get_completion_time() {
	return completion_time;
}

float Job::get_p_over_alpha() {
	return float(processing_time)/earliness_penalty;
}

float Job::get_p_over_beta() {
	return float(processing_time)/tardiness_penalty;
}

std::ostream& operator<< (std::ostream& stream, const Job& job) {
	if (job.completion_time <= job.due_date)
		stream << "{" << job.name << ":\t" << job.starting_time << "\t" << job.completion_time << "\t" << job.due_date << "\t" << job.earliness_penalty << "\t" << job.tardiness_penalty << "\t" << (job.due_date - job.completion_time)*job.earliness_penalty << " }***";
	else
		stream << "{" << job.name << ":\t" << job.starting_time << "\t" << job.completion_time << "\t" << job.due_date << "\t" << job.earliness_penalty << "\t" << job.tardiness_penalty << "\t" << (job.completion_time - job.due_date)*job.tardiness_penalty << " }";
	//stream << "{" << job.name << ": " << job.starting_time << ", " << job.processing_time << ", " << job.completion_time << "}";
	return stream;
}

Job& Job::operator=(const Job& operand) {
	if (&operand == this)
		return *this;

	name = operand.name;
	due_date = operand.due_date;
	processing_time = operand.processing_time;
	earliness_penalty = operand.earliness_penalty;
	tardiness_penalty = operand.tardiness_penalty;
	starting_time = operand.starting_time;
	completion_time = operand.completion_time;

	return *this;
}


int calc_due_date(Job MyJobs[],float h, int n) {
	int sum_p = 0;

	for (int i=0; i<n; i++) {
		sum_p += MyJobs[i].get_processing_time();
	}

	return floor(sum_p*h);
}

void set_due_date(Job MyJobs[], int due_date, int n) {
	for (int i=0; i<n; i++) {
		MyJobs[i].set_due_date(due_date);
	}
}




void order_by(Job MyJobs[], std::string expression, std::string mode, int n) {
	PostfixCalculator p;
	std::string element = "";
	const char space = 32; // space ASCII value
	expression += space;
	//Job* SortedJobs = new Job[n];
	Job aux_job;
	float expression_results[n];
	int job_keys[n], aux_key;
	float aux;

	for (int j=0; j < n; j++) {
		// calculate expression
		for (int i=0; i < expression.length(); i++) {
            // if you found a space, that means the element is over: do operation or add to stack
            if ((strncmp(&expression.at(i),&space,1) == 0)) {

                  if (element == "+")
                        p.add();
                  else if (element == "-")
                        p.sub();
                  else if (element == "/")
                        p.div();
                  else if (element == "*")
                        p.mul();
                  else if (element == "~")
                        p.neg();
                  else {
                  		if (element == "a" || element == "A")
                        	p.push(MyJobs[j].get_earliness_penalty());
                        if (element == "b" || element == "B")
                        	p.push(MyJobs[j].get_tardiness_penalty());
                        if (element == "p" || element == "P")
                        	p.push(MyJobs[j].get_processing_time());
                  }

                  //cout << element << endl;
                  element.erase();
            }
            else // if is not an space, keep concatenating, beucase it can be a multi-digit number
                  element += expression.at(i);
		}
		// get results
		expression_results[j] = p.top();
		job_keys[j] = MyJobs[j].get_name();
		//std::cout << j+1 << ": " << expression_results[j] << std::endl;
	}

	for (int i=0; i<n-1; i++) {
		for (int j=i+1; j<n; j++) {
			if (mode == "increasing") {
				if (expression_results[j] < expression_results[i]) {
					aux = expression_results[i];
					expression_results[i] = expression_results[j];
					expression_results[j] = aux;
					
					aux_job = MyJobs[i];
					MyJobs[i] = MyJobs[j];
					MyJobs[j] = aux_job;

					aux_key = job_keys[i];
					job_keys[i] = job_keys[j];
					job_keys[j] = aux_key;
					
				} 
			} else if (mode == "decreasing") {
				if (expression_results[j] > expression_results[i]) {
					aux = expression_results[i];
					expression_results[i] = expression_results[j];
					expression_results[j] = aux;
					
					aux_job = MyJobs[i];
					MyJobs[i] = MyJobs[j];
					MyJobs[j] = aux_job;
					
					aux_key = job_keys[i];
					job_keys[i] = job_keys[j];
					job_keys[j] = aux_key;
				} 
			}
		}
	}

	//std::cout << std::endl << std::endl;

	//for (int i=0; i<n; i++)
	//	std::cout << job_keys[i] << ": " << expression_results[i] << std::endl;

}



