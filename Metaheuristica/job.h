#ifndef JOB_H
#define JOB_H
#include <iostream>
#include <cmath>
#define NOT_INITIALIZED -1
#define NOT_ABLE_TO_ALLOCATE -1
#define JOB_ALREADY_IN_SCHEDULE -2

class Job {
private:
	int name;
	int due_date;
	int processing_time;
	int earliness_penalty;
	int tardiness_penalty;
	int starting_time;
	int completion_time;
	
public:
	Job(int n, int d, int pi, int Ai, int Bi);
	Job();
	~Job();

	void set_name(int n);
	void set_due_date(int d);
	void set_processing_time(int pi);
	void set_earliness_penalty(int Ai);
	void set_tardiness_penalty(int Bi);
	void set_starting_time(int s);
	void set_completion_time(int C);

	int get_name();
	int get_due_date();
	int get_processing_time();
	int get_earliness_penalty();
	int get_tardiness_penalty();
	int get_starting_time();
	int get_completion_time();
	float get_p_over_alpha();
	float get_p_over_beta();


	friend std::ostream& operator<< (std::ostream& stream, const Job& job);
	Job& operator=(const Job&);
};

int calc_due_date(Job*,float,int);
void set_due_date(Job*,int,int);
void order_by(Job*,std::string,std::string,int);
#endif