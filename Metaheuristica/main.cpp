#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>
#include <iomanip>
#include <vector>
#include "job.h"
#include "schedule.h"
#include <iomanip>

using namespace std;

bool FILE_OPENED = false;
string filename;
int N;

bool compareSchedule (Schedule i,Schedule j) { 
	return (i.get_objective_function()<j.get_objective_function()); 
}

bool compareJob (Job i,Job j) { 
	return (i.get_name()<j.get_name()); 
}

bool find_al(int j,std::vector<int> allocated) {
	for (int i=0; i<allocated.size(); i++)
		if (j == allocated.at(i))
			return true;

	return false;
}

bool find_al(int j,std::vector<Job> allocated) {
	for (int i=0; i<allocated.size(); i++)
		if (j == allocated.at(i).get_name())
			return true;

	return false;
}

Job* read_from_file(int* n) {
	static ifstream file;
	Job* MyJobs;
	int aux, j=0;

	if (!FILE_OPENED) {
		file.open(filename);

		if (!file.is_open()) {
			cout << "File not found." << endl;
			exit(1);
		} else {
			FILE_OPENED = true;
		}

		file >> *n;
	}

	file >> *n;

	MyJobs = new Job[*n];

	while (j<*n) {
		MyJobs[j].set_name(j+1);
		file >> aux;
		MyJobs[j].set_processing_time(aux);
		file >> aux;
		MyJobs[j].set_earliness_penalty(aux);
		file >> aux;
		MyJobs[j].set_tardiness_penalty(aux);
		j++;
	}

	return MyJobs;
}

void print_Jobs(Job MyJobs[], int n) {
	for (int i=0; i<n; i++) {
		cout << MyJobs[i].get_name() << " ";
		cout << MyJobs[i].get_processing_time() << " ";
		cout << MyJobs[i].get_earliness_penalty() << " ";
		cout << MyJobs[i].get_tardiness_penalty() << endl;
	}
}

Job* copy_jobs(Job MyJobs[], int n) {
	Job* MyJobs2 = new Job[n];
	for (int i=0; i<n; i++)
		MyJobs2[i] = MyJobs[i];
	return MyJobs2;
}

/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */
//																	//
//	compile with: g++ *.cpp -o main -Wall							//
//																	//
//	where cpp files are: main.cpp, job.cpp, schedule.cpp 			//
//	and calculator.cpp 												//
//																	//
/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */
//																	//
//	to run:	./executable schXYZ.txt 								//
//																	//
//	where schXYZ is one of sch10.txt, sch20.txt, ..., sch1000.txt	//
//																	//
/* - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - */

int main(int argc, char** argv) {
	int n, due_date, test_before, test_after;
	int pointer_before=0, pointer_after=0;
	bool ALLOWED_TO_PROCEED;
	ofstream file_out, file2_out;

	string filename_z = argv[1];
	filename = "input_files/"+filename_z;
	
	Job* MyJobs = read_from_file(&n);
	Job* MyJobs2;
	N = n;

	Job* Ordered_Jobs;
	Ordered_Jobs = copy_jobs(MyJobs,n);

	string filename_out = "output_files/saida_C_"+filename_z;
	string filename2_out = "output_files/saida_MH_"+filename_z;

	file_out.open(filename_out);
	file2_out.open(filename2_out);

	file_out << "\th=0.2\th=0.4\th=0.6\th=0.8" << endl;
	file2_out << "\th=0.2\th=0.4\th=0.6\th=0.8" << endl;

	for (int k=0; k<10; k++) {
		file_out << "k = " << k+1 << "\t";
		file2_out << "k = " << k+1 << "\t";
		for (float h=0.2; h < 1.0; h+=0.2) {
			cout << ">>>>> >>>> >>> >> (k = " << k+1 << " | h = " << h << ") << <<< <<<< <<<<<" << endl;

			due_date = calc_due_date(MyJobs, h, n);		// calculate due date based on all available jobs and h
			set_due_date(MyJobs,due_date,n);			// update due date for every job 
			MyJobs2 = copy_jobs(MyJobs,n);
			
			Job* Ordered_Jobs;
			Ordered_Jobs = copy_jobs(MyJobs,n);

			order_by(MyJobs,"p A /", "decreasing", n);
			order_by(MyJobs2,"p B /", "decreasing", n);
			
			Schedule MySchedule(n,due_date);
			MySchedule.add_first_job(MyJobs2[0],due_date,'S');
			pointer_after++;
			
			while (MySchedule.get_number_of_scheduled_jobs() < n) {

				ALLOWED_TO_PROCEED = true;
				test_before = MySchedule.test_add_job(MyJobs[pointer_before],'X');
				test_after = MySchedule.test_add_job(MyJobs2[pointer_after],'Y');

				if (test_before == NOT_ABLE_TO_ALLOCATE && pointer_before < n) {
					pointer_before++;
					ALLOWED_TO_PROCEED = false;
					//cout << "test_before: (unable to allocate)" << endl;
				} else if (test_before == JOB_ALREADY_IN_SCHEDULE && pointer_before < n) {
					pointer_before++;
					ALLOWED_TO_PROCEED = false;
					//cout << "test_before: (job already scheduled)" << endl;
				}

				if (test_after == JOB_ALREADY_IN_SCHEDULE && pointer_after < n) {
					pointer_after++;
					ALLOWED_TO_PROCEED = false;
					//cout << "test_after: (job already scheduled)" << endl;
				}


				if (ALLOWED_TO_PROCEED) {
					if (pointer_before >= n) {
						MySchedule.add_job(MyJobs2[pointer_after],'Y');
						pointer_after++;
					} else {
						if (test_before <= test_after) {
							MySchedule.add_job(MyJobs[pointer_before],'X');
							pointer_before++;
						} else {
							MySchedule.add_job(MyJobs2[pointer_after],'Y');
							pointer_after++;
						}
					}
				}
			}
			pointer_before = 0;
			pointer_after = 0;

			//cout << MySchedule << ": " << MySchedule.get_objective_function() << endl << endl;
			file_out << MySchedule.get_objective_function() << "\t";

			/* - - - - - - - - - - - - - */
			// METAHEURISTICS START HERE //
			// METAHEURISTICS START HERE //
			// METAHEURISTICS START HERE //
			// METAHEURISTICS START HERE //
			/* - - - - - - - - - - - - - */

			cout << "Initial solution:\t";
			Schedule AntSchedule = MySchedule;
			cout << AntSchedule.get_objective_function() << ": " << AntSchedule << endl;

			// step 1.1 - initalize generation counter to 0 and other important variables
			int g = 0;					// generation counter
			int n_ants = 20; 			// number of ants
			int G = 10;		 			// maximum number of colony generations
			double evaporation = 0.1;	// evaporation rate

			// step 1.2 - create an initial AntColony of size n_ants
			std::vector<Schedule> AntColony(n_ants);

			Schedule Ant;
			std::vector<int> early_jobs = AntSchedule.get_jobs("early");
			std::vector<int> late_jobs = AntSchedule.get_jobs("late");
			bool equal_cond;

			for (unsigned k=0; k<n_ants; k++) {
				AntColony[k] = AntSchedule;
				for (unsigned i=0; i<early_jobs.size(); i++) {
					for (unsigned j=0; j<late_jobs.size(); j++) {
						
						Ant = AntSchedule;
						double r = double(rand())/RAND_MAX; // generate random number

						if (r > 0.95) {
							Ant.swap(early_jobs.at(i),late_jobs.at(j));
							Ant.apply_Vshape();
							
							equal_cond = false;
							for (int c=0; c<k; c++)
								if (Ant.get_objective_function() == AntColony.at(c).get_objective_function())
									equal_cond = true;

							if (!equal_cond)
								if (Ant.get_objective_function() >= 0.90*AntSchedule.get_objective_function() && Ant.get_objective_function() <= 1.1*AntSchedule.get_objective_function())
									AntColony.at(k) = Ant;
						}
					}
				}
			}

			std::sort(AntColony.begin(), AntColony.end(), compareSchedule);
			std::vector<int> job_names;
			
			// step 1.3 - set Best Ant in colony
			Schedule BestAnt = AntColony.at(0);

			// step 1.4 - initilize feromone matrix
			double feromone_level[n][n];
			for (int j=0; j<n; j++)
				for (int k=0; k<n; k++)
					feromone_level[j][k] = 0.0;

			for (int k=0; k<max(min(5,n_ants),min(int(sqrt(n)),int(n_ants/2))); k++){
				job_names = AntColony.at(k).get_job_names();
				for (int i=0; i<n; i++)
					for (int j=0; j<n; j++)
						if (i+1 == job_names.at(j))
							feromone_level[i][j]++;
			}

			//cout << endl << "Feromone Matrix" << endl;;
			// SHOW FEROMONE LEVEL
			/*
			for (int j=0; j<n; j++) {
				for (int k=0; k<n; k++) {
					cout << feromone_level[j][k] << " ";
				}
				cout << endl;
			}
			*/

			// step 2 - iterative
			while (g < G) {
				// step 2.1 - increment generation counter
				g++;

				// step 2.2 - build colony of ants based on feromone and attractiveness
				std::vector<Job> Na = AntSchedule.get_list_of_jobs();
				std::vector<Schedule> AntColony_new(n_ants);

				int ants_in_colony = 0;
				
				while (ants_in_colony < n_ants) {

					std::vector<Job> Na = AntSchedule.get_list_of_jobs();
					std::vector<Job> S = AntSchedule.get_list_of_jobs();
					std::sort(S.begin(), S.end(), compareJob);

					int ant_counter = 0;
					double st = Na.at(0).get_starting_time();
					std::vector<int> i_allocated;
					std::vector<Job> j_allocated(n);

					while (ant_counter < n) {
						ant_counter++;

						std::sort(Na.begin(), Na.end(), compareJob);

						double attractiveness[n][n];
						for (int i=0; i<n; i++)
							for (int j=0; j<n; j++)
								attractiveness[i][j] = 0.0;

						double alpha_over_beta = 0.0;
						double beta_over_alpha = 0.0;

						for (int i=0; i<Na.size(); i++) {
							alpha_over_beta += double(Na.at(i).get_earliness_penalty())/Na.at(i).get_tardiness_penalty();
							beta_over_alpha += double(Na.at(i).get_tardiness_penalty())/Na.at(i).get_earliness_penalty();
						}

						for (int i=0; i<n; i++)
							for (int j=0; j<n; j++) {
								if (double(j)/n >= h)
									attractiveness[i][j] = double(S.at(i).get_earliness_penalty())/S.at(i).get_tardiness_penalty() / alpha_over_beta;
								else
									attractiveness[i][j] = double(S.at(i).get_tardiness_penalty())/S.at(i).get_earliness_penalty() / beta_over_alpha;
							}

						/*
						cout << endl << "showing ATTRACTIVENESS: " << endl; 
						for (int i=0; i<n; i++) {
							for (int j=0; j<n; j++) {
								cout << std::fixed << setprecision(2) << attractiveness[i][j] << "\t";
							}
							cout << endl;
						}
						*/
						
						double p[n][n];
						for (int i=0; i<n; i++)
							for (int j=0; j<n; j++)
								p[i][j] = 0.0;


						double sum_fer_atrac;
						
						
						sum_fer_atrac = 0;

						for (int i=0; i<n; i++)
							for (int j=0; j<Na.size(); j++)
								sum_fer_atrac += feromone_level[Na.at(j).get_name()-1][i] * attractiveness[Na.at(j).get_name()-1][i];

						for (int i=0; i < n; i++)
							for (int j=0; j<n; j++)
								p[i][j] = feromone_level[i][j]*attractiveness[i][j]/sum_fer_atrac;

						double r = double(rand())/RAND_MAX;

						double p_max = -1;
						int position = -1;
						Job job;

						for (int i=0; i < n; i++) {
							if (!find_al(i+1,j_allocated)) {
								for (int j=0; j<n; j++) {
									if (!find_al(j,i_allocated)) {
										if (r < 0.60) {

											if (p[i][j] > p_max) {
												p_max = p[i][j];
												job = S.at(i);
												position = j;
											}
											//cout << std::fixed << setprecision(2) << p[i][j] << "\t";
										} else {
											
											if (attractiveness[i][j] > p_max) {
												p_max = attractiveness[i][j];
												job = S.at(i);
												position = j;
											}
											//cout << std::fixed << setprecision(2) << attractiveness[i][j] << "\t";
										}
									}
								}
								//cout << endl;
							}
						}
						
						i_allocated.push_back(position);
						j_allocated[position] = job;

						for (int i=0; i<Na.size(); i++)
							if (Na.at(i).get_name() == job.get_name()) {
								Na.erase(Na.begin()+i);
								break;
							}

						Schedule Ant_in_Colony;
					}

					Schedule temp(n,due_date);

					for (int i=0; i<n; i++)
						temp.add_first_job(j_allocated[i],st+i,'S');

					temp.update_times();
					temp.apply_Vshape();

					AntColony_new.at(ants_in_colony) = temp;
					ants_in_colony++;
				}
			
				// step 2.3 - Merging the best n_ants in Colonies AntColony and Any_Colony_new 
				AntColony.insert(AntColony.end(), AntColony_new.begin(), AntColony_new.end());
				std::sort(AntColony.begin(), AntColony.end(), compareSchedule);

				for (int i=0; i<n_ants; i++)
					AntColony.pop_back();

				for (int i=0; i<n_ants; i++)
					AntColony.at(i).fix_pointers();

				// step 2.4 - Select the best ant + Local Search
				BestAnt = AntColony.at(0);

				for (int i=0; i<n_ants; i++) {
					Schedule FatherSchedule = AntColony.at(i);
					Schedule BestSchedule = FatherSchedule;

					early_jobs = FatherSchedule.get_jobs("early");
					late_jobs = FatherSchedule.get_jobs("late");

					// limited local search starts here
					for (unsigned j=0; j<min(int(early_jobs.size()),5); j++) {
						for (unsigned k=0; k<min(int(late_jobs.size()),10); k++) {
							Schedule ChildSchedule = FatherSchedule;
							ChildSchedule.swap(early_jobs.at(j),late_jobs.at(k));
							ChildSchedule.apply_Vshape();

							if (ChildSchedule.get_objective_function() < BestSchedule.get_objective_function())
								BestSchedule = ChildSchedule;
						}
					}

					AntColony.at(i) = BestSchedule;
					if (AntColony.at(i).get_objective_function() < BestAnt.get_objective_function())
						BestAnt = AntColony.at(i);
				}
				
				// step 2.5 - Determine new feromone level
				double feromone_level_new[n][n];
				for (int j=0; j<n; j++)
					for (int k=0; k<n; k++)
						feromone_level_new[j][k] = 0.0;

				for (int k=0; k<max(min(5,n_ants),min(int(sqrt(n)),int(n_ants/2))); k++){					
					job_names = AntColony.at(k).get_job_names();
					for (int i=0; i<n; i++)
						for (int j=0; j<n; j++)
							if (i+1 == job_names.at(j))
								feromone_level_new[i][j]++;
				}

				for (int j=0; j<n; j++)
					for (int k=0; k<n; k++)
						feromone_level[j][k] = (1-evaporation)*feromone_level[j][k] + feromone_level_new[j][k];				
			}

			cout << "Ant Colony solution:\t" << BestAnt.get_objective_function() << ": " << BestAnt << endl << endl;
			
			std::vector<Job> Na = BestAnt.get_list_of_jobs();
			
			//////////////////////////////////////////////////////////////////////////////////
			//																				//
			// uncomment below to see the complete schedule (ant not only the sequence)		//
			// in the format:  																//
			// 																				//
			// {job_name: start_time, completion_time, due_date, alpha, beta, lateness} 	//
			// 																				//
			//////////////////////////////////////////////////////////////////////////////////

			//for (int i=0; i<n; i++)
			//	cout << Na[i] << endl;

			file2_out << BestAnt.get_objective_function() << "\t";
		}

		file_out << endl;
		file2_out << endl;
		MyJobs = read_from_file(&n);
	}

	file_out.close();
	file2_out.close();

	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */
	/* Generating Comparison */

	string files_out[7] = {"saida_C_sch10.txt", "saida_C_sch20.txt", "saida_C_sch50.txt", "saida_C_sch100.txt", "saida_C_sch200.txt", "saida_C_sch500.txt", "saida_C_sch1000.txt"};
	string files_out2[7] = {"saida_MH_sch10.txt", "saida_MH_sch20.txt", "saida_MH_sch50.txt", "saida_MH_sch100.txt", "saida_MH_sch200.txt", "saida_MH_sch500.txt", "saida_MH_sch1000.txt"};
	string files_comp[7] = {"biskup_10.txt", "biskup_20.txt", "biskup_50.txt", "biskup_100.txt", "biskup_200.txt", "biskup_500.txt", "biskup_1000.txt"};
	string my_f_out[7] = {"tabela_linda_C_10b.txt", "tabela_linda_C_20b.txt", "tabela_linda_C_50b.txt", "tabela_linda_C_100b.txt", "tabela_linda_C_200b.txt", "tabela_linda_C_500b.txt", "tabela_linda_C_1000b.txt"};
	string my_f_out2[7] = {"tabela_linda_MH_10b.txt", "tabela_linda_MH_20b.txt", "tabela_linda_MH_50b.txt", "tabela_linda_MH_100b.txt", "tabela_linda_MH_200b.txt", "tabela_linda_MH_500b.txt", "tabela_linda_MH_1000b.txt"};
	string my_f_out3[7] = {"tabela_linda_CvsM_10b.txt", "tabela_linda_CvsM_20b.txt", "tabela_linda_CvsM_50b.txt", "tabela_linda_CvsM_100b.txt", "tabela_linda_CvsM_200b.txt", "tabela_linda_CvsM_500b.txt", "tabela_linda_CvsM_1000b.txt"};
	string discard;
	ifstream f1, f2, f4;
	ofstream f3, f5, f6;

	for (int i=0; i<7; i++) {
		files_out[i] = "output_files/" + files_out[i];
		files_out2[i] = "output_files/" + files_out2[i];
		my_f_out[i] = "output_files/" + my_f_out[i];
		my_f_out2[i] = "output_files/" + my_f_out2[i];
		my_f_out3[i] = "output_files/" + my_f_out3[i];
		files_comp[i] = "input_files/" + files_comp[i];
	}

	double f1_metrics[40], f2_metrics[40], f4_metrics[40], comparison[40], comparison2[40], comparison3[40];
	double mean=0, mean2=0, mean3=0, sigma=0, sigma2=0, sigma3=0;
	
	int n_files;
	
	if (filename == "input_files/sch10.txt")
		n_files = 1;
	else if (filename == "input_files/sch20.txt")
		n_files = 2;
	else if (filename == "input_files/sch50.txt")
		n_files = 3;
	else if (filename == "input_files/sch100.txt")
		n_files = 4;
	else if (filename == "input_files/sch200.txt")
		n_files = 5;
	else if (filename == "input_files/sch500.txt")
		n_files = 6;
	else if (filename == "input_files/sch1000.txt")
		n_files = 7;
	else n_files = 0;

	for (int i=0; i<n_files; i++) {
		f1.open(files_out[i]);
		f2.open(files_comp[i]);
		f4.open(files_out2[i]);
	
		for (int j=0; j<4; j++) {
			f1 >> discard;
			f4 >> discard;
		}

		for (int j=0; j<13; j++)
			f2 >> discard;

		for (int j=0; j<10; j++) {
			f1 >> discard >> discard >> discard;
			f4 >> discard >> discard >> discard;
			f2 >> discard >> discard >> discard;

			for (int k=0; k<4; k++) {
				f1 >> discard;
				f1_metrics[4*j+k] = stoi(discard);

				f4 >> discard;
				f4_metrics[4*j+k] = stoi(discard);

				f2 >> discard;
				discard.erase(std::remove(discard.begin(), discard.end(), ','), discard.end());
				f2_metrics[4*j+k] = stoi(discard);
			}
		}

		f1.close();
		f4.close();
		f2.close();
		
	}
	
	for (int j=0; j<10; j++) {
		for (int k=0; k<4; k++) {
			comparison[4*j+k] = (f1_metrics[4*j+k] - f2_metrics[4*j+k])/f2_metrics[4*j+k] * 100;
			comparison2[4*j+k] = (f4_metrics[4*j+k] - f2_metrics[4*j+k])/f2_metrics[4*j+k] * 100;
			comparison3[4*j+k] = (f4_metrics[4*j+k] - f1_metrics[4*j+k])/f1_metrics[4*j+k] * 100;
			mean += comparison[4*j+k];
			mean2 += comparison2[4*j+k];
			mean3 += comparison3[4*j+k];
		}
	}

	mean /= 40.0;
	mean2 /= 40.0;
	mean3 /= 40.0;

	for (int i=0; i<40; i++) {
		sigma += pow(mean-comparison[i],2);
		sigma2 += pow(mean2-comparison2[i],2);
		sigma3 += pow(mean3-comparison3[i],2);
	}

	sigma = sqrt(sigma/(40-1));
	sigma2 = sqrt(sigma2/(40-1));
	sigma3 = sqrt(sigma3/(40-1));

	cout << endl;
	cout << "N~(" << mean << "," << sigma << ")" << endl;
	cout << "N~(" << mean2 << "," << sigma2 << ")" << endl;
	cout << "N~(" << mean3 << "," << sigma3 << ")" << endl;

	std::cout.precision(2);
	
	f3.open(my_f_out[n_files-1]);
	f5.open(my_f_out2[n_files-1]);
	f6.open(my_f_out3[n_files-1]);

	for (int j=0; j<10; j++) {
		for (int k=0; k<4; k++) {
			f3 << std::setprecision(0) << std::fixed << "(" << f1_metrics[4*j+k] << "/" << f2_metrics[4*j+k] << ") " << std::setprecision(1) << comparison[4*j+k] << "%\t";
			f5 << std::setprecision(0) << std::fixed << "(" << f4_metrics[4*j+k] << "/" << f2_metrics[4*j+k] << ") " << std::setprecision(1) << comparison2[4*j+k] << "%\t";
			f6 << std::setprecision(0) << std::fixed << "(" << f4_metrics[4*j+k] << "/" << f1_metrics[4*j+k] << ") " << std::setprecision(1) << comparison3[4*j+k] << "%\t";
		}
		f3 << endl;
		f5 << endl;
		f6 << endl;
	}

	f3.close();
	f5.close();
	f6.close();


	delete[] MyJobs;
	delete[] MyJobs2;

	return 0;
	
}
