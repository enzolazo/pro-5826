#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <iostream>
#include <list>
#include "job.h"
#include <vector>
#define NOT_INITIALIZED -1

class Schedule {
private:
	std::list<Job> MySchedule;
	std::list<Job>::iterator head;
	std::list<Job>::iterator tail;
	int n;
	
	int starting_time;
	int number_of_scheduled_jobs;
	int number_of_anticipated_jobs;

public:

	int due_date;

	
	Schedule(int n_jobs, int d);
	Schedule();
	~Schedule();
	

	void add_first_job(Job first_job, int time, char mode);
	void set_due_date(int d);
	void add_job(Job job, char mode);
	int test_add_job(Job job, char mode);
	bool job_in_schedule(Job job);
	int get_number_of_scheduled_jobs();
	long int get_objective_function();
	void fix_pointers();
	void swap(int id_a, int id_b);
	int get_starting_time();
	void update_times();
	void apply_Vshape();
	std::vector<int> get_jobs(std::string mode);
	std::vector<int> get_job_names();

	std::list<Job>::iterator get_schedule_initial_position() const;
	std::list<Job>::iterator get_schedule_last_position() const;
	std::list<Job> get_list();
	std::vector<Job> get_list_of_jobs();

	friend std::ostream& operator<< (std::ostream& stream, const Schedule& schedule);
	Schedule& operator=(const Schedule&);
};

#endif